#include <iostream>

using namespace std;

#define CARRIAGES 10
#define MAX_PASSENGERS 20

#define FILLING(carr)   int i = 0;                      \
                        while(i < CARRIAGES){           \
                            int num;                    \
                            cin >> num;                 \
                            if(num < 0)                 \
                                cout << "Wrong input";  \
                            else{                       \
                                *(carr + i) = num;      \
                                i++;                    \
                            }                           \
                        }

#define CARRIAGE_TEST(carr, func)   for(int i = 0; i < CARRIAGES; i++){ \
                                        if(func(*(carr + i))){          \
                                            cout << i + 1 << " ";       \
                                        }                               \
                                    }

#define NUM_PASSENGERS(carr)    int num = 0;                        \
                                for(int i = 0; i < CARRIAGES; i++){ \
                                    num += *(carr + i);             \
                                }                                   \
                                cout << num;

bool IsOverflow(int passengers){
    return passengers > MAX_PASSENGERS ? true : false;
}

bool IsFreePlaces(int passengers){
    return passengers < MAX_PASSENGERS ? true : false;
}

int main()
{
    int carriages[CARRIAGES];

    FILLING(carriages);
    cout << "Overflowing carriages: ";
    CARRIAGE_TEST(carriages, IsOverflow);
    cout << endl;
    cout << "Carriages with free places: ";
    CARRIAGE_TEST(carriages, IsFreePlaces);
    cout << endl;
    cout << "Nuber of passengers: ";
    NUM_PASSENGERS(carriages);
    cout << endl;

    return 0;
}
