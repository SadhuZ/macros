#include <iostream>

using namespace std;

#define SPRING 1
#define SUMMER 0
#define AUTUMN 0
#define WINTER 1

#define SUMMER_

int main()
{
#if SPRING
    cout << "SPRING" << endl;
#endif
#if SUMMER
    cout << "SUMMER" << endl;
#endif
#if AUTUMN
    cout << "AUTUMN" << endl;
#endif
#if WINTER
    cout << "WINTER" << endl;
#endif

#ifdef SPRING_
    cout << "SPRING" << endl;
#endif
#ifdef SUMMER_
    cout << "SUMMER" << endl;
#endif
#ifdef AUTUMN_
    cout << "AUTUMN" << endl;
#endif
#ifdef WINTER_
    cout << "WINTER" << endl;
#endif


    return 0;

}
