#include <iostream>

using namespace std;

#define DAY1 "Sunday"
#define DAY2 "Monday"
#define DAY3 "Tuesday"
#define DAY4 "Wednesday"
#define DAY5 "Thursday"
#define DAY6 "Friday"
#define DAY7 "Saturday"
#define PRINT_DAYOFWEEK(num)    if(num == 1) cout << DAY1 << endl; \
                                else if(num == 2) cout << DAY2 << endl; \
                                else if(num == 3) cout << DAY3 << endl; \
                                else if(num == 4) cout << DAY4 << endl; \
                                else if(num == 5) cout << DAY5 << endl; \
                                else if(num == 6) cout << DAY6 << endl; \
                                else if(num == 7) cout << DAY7 << endl;
int main()
{
    int numDay;
    cin >> numDay;
    PRINT_DAYOFWEEK(numDay);
    return 0;
}
